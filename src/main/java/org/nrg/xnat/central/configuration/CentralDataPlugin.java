package org.nrg.xnat.central.configuration;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xdat.om.CndaAdrcPsychometrics;
import org.nrg.xdat.om.CndaAtlasscalingfactordata;
import org.nrg.xdat.om.CndaAtrophynildata;
import org.nrg.xdat.om.CndaAtrophynildataPeak;
import org.nrg.xdat.om.CndaClinicalassessmentdata;
import org.nrg.xdat.om.CndaClinicalassessmentdataDiagnosis;
import org.nrg.xdat.om.CndaClinicalassessmentdataMedication;
import org.nrg.xdat.om.CndaCndasubjectmetadata;
import org.nrg.xdat.om.CndaCsfdata;
import org.nrg.xdat.om.CndaDiagnosesdata;
import org.nrg.xdat.om.CndaDtidata;
import org.nrg.xdat.om.CndaDtiregion;
import org.nrg.xdat.om.CndaHandednessdata;
import org.nrg.xdat.om.CndaLevelsdata;
import org.nrg.xdat.om.CndaManualvolumetrydata;
import org.nrg.xdat.om.CndaManualvolumetryregion;
import org.nrg.xdat.om.CndaManualvolumetryregionSlice;
import org.nrg.xdat.om.CndaModifiedscheltensdata;
import org.nrg.xdat.om.CndaModifiedscheltenspvregion;
import org.nrg.xdat.om.CndaModifiedscheltensregion;
import org.nrg.xdat.om.CndaPettimecoursedata;
import org.nrg.xdat.om.CndaPettimecoursedataDuration;
import org.nrg.xdat.om.CndaPettimecoursedataDurationBp;
import org.nrg.xdat.om.CndaPettimecoursedataRegion;
import org.nrg.xdat.om.CndaPettimecoursedataRegionActivity;
import org.nrg.xdat.om.CndaPsychometricsdata;
import org.nrg.xdat.om.CndaRadiologyreaddata;
import org.nrg.xdat.om.CndaSegmentationfastdata;
import org.nrg.xdat.om.CndaTicvideocountsdata;
import org.nrg.xdat.om.CndaVitalsdata;
import org.nrg.xdat.om.CndaVitalsdataBloodpressure;
import org.nrg.xdat.om.CndaVitalsdataPulse;
import org.nrg.xdat.om.CndaVolumetryregioninfo;
/*import org.nrg.xdat.om.FsAparcregionanalysis;
import org.nrg.xdat.om.FsAparcregionanalysisHemisphere;
import org.nrg.xdat.om.FsAparcregionanalysisHemisphereRegion;
import org.nrg.xdat.om.FsAsegregionanalysis;
import org.nrg.xdat.om.FsAsegregionanalysisRegion;
import org.nrg.xdat.om.FsAutomaticsegmentationdata;
import org.nrg.xdat.om.FsFsdata;
import org.nrg.xdat.om.FsFsdataHemisphere;
import org.nrg.xdat.om.FsFsdataHemisphereRegion;
import org.nrg.xdat.om.FsFsdataRegion;
import org.nrg.xdat.om.FsLongfsdata;
import org.nrg.xdat.om.FsLongfsdataHemisphere;
import org.nrg.xdat.om.FsLongfsdataHemisphereRegion;
import org.nrg.xdat.om.FsLongfsdataRegion;
import org.nrg.xdat.om.FsLongfsdataTimepoint;*/
import org.nrg.xdat.om.GeneticsGenetictestresults;
import org.nrg.xdat.om.GeneticsGenetictestresultsGene;
import org.nrg.xdat.om.NeurocogSymptomsneurocog;
import org.nrg.xdat.om.NundaNundademographicdata;
import org.nrg.xdat.om.NundaNundademographicdataRelationship;
import org.nrg.xdat.om.SapssansSymptomssapssans;
import org.nrg.xdat.om.SfEncounterlog;
import org.nrg.xdat.om.SfEncounterlogEncounter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@XnatPlugin(value = "central-datatypes-plugin",
            name = "XNAT Central Data Types Plugin",
            dataModels = {
                    @XnatDataModel(value = CndaAdrcPsychometrics.SCHEMA_ELEMENT_NAME,
                                         singular = "ADRC Psychometric Data",
                                         plural = "ADRC Psychometric Data"),
                    @XnatDataModel(value = CndaAtlasscalingfactordata.SCHEMA_ELEMENT_NAME,
                                         singular = "ASF",
                                         plural = "ASFs"),
                    @XnatDataModel(value = CndaAtrophynildata.SCHEMA_ELEMENT_NAME,
                                         singular = "Atrophy Nil Data",
                                         plural = "Atrophy Nil Data"), 
                    @XnatDataModel(value = CndaAtrophynildataPeak.SCHEMA_ELEMENT_NAME,
                                         singular = "Atrophy Nil Data Peak",
                                         plural = "Atrophy Nil Data Peaks"),
                    @XnatDataModel(value = CndaClinicalassessmentdata.SCHEMA_ELEMENT_NAME,
                                         singular = "CLIN",
                                         plural = "CLINs"),
                    @XnatDataModel(value = CndaClinicalassessmentdataDiagnosis.SCHEMA_ELEMENT_NAME,
                                         singular = "CLIN Diagnosis",
                                         plural = "CLIN Diagnoses"),
                    @XnatDataModel(value = CndaClinicalassessmentdataMedication.SCHEMA_ELEMENT_NAME,
                                         singular = "CLIN Medication",
                                         plural = "CLIN Medications"),
                    @XnatDataModel(value = CndaCndasubjectmetadata.SCHEMA_ELEMENT_NAME,
                                         singular = "CNDA Subject Metadata",
                                         plural = "CNDA Subject Metadata"), 
                    @XnatDataModel(value = CndaCsfdata.SCHEMA_ELEMENT_NAME,
                                         singular = "CSF",
                                         plural = "CSFs"),
                    @XnatDataModel(value = CndaDiagnosesdata.SCHEMA_ELEMENT_NAME,
                                         singular = "Diagnoses Data",
                                         plural = "Diagnoses Data"),
                    @XnatDataModel(value = CndaDtidata.SCHEMA_ELEMENT_NAME,
                                         singular = "DTI",
                                         plural = "DTIs"),                                                             
                    @XnatDataModel(value = CndaDtiregion.SCHEMA_ELEMENT_NAME,
                                         singular = "DTI Region",
                                         plural = "DTI Regions"),
                    @XnatDataModel(value = CndaHandednessdata.SCHEMA_ELEMENT_NAME,
                                         singular = "Handedness Data",
                                         plural = "Handedness Data"),
                    @XnatDataModel(value = CndaLevelsdata.SCHEMA_ELEMENT_NAME,
                                         singular = "Levels Data",
                                         plural = "Levels Data"),                                                                                  
                    @XnatDataModel(value = CndaManualvolumetrydata.SCHEMA_ELEMENT_NAME,
                                         singular = "Manual Volumetry",
                                         plural = "Manual Volumetries"),
                    @XnatDataModel(value = CndaManualvolumetryregion.SCHEMA_ELEMENT_NAME,
                                         singular = "Manual Volumetry Region",
                                         plural = "Manual Volumetry Regions"),                                                                     
                    @XnatDataModel(value = CndaManualvolumetryregionSlice.SCHEMA_ELEMENT_NAME,
                                         singular = "Manual Volumetry Region Slice",
                                         plural = "Manual Volumetry Region Slices"), 
                    @XnatDataModel(value = CndaModifiedscheltensdata.SCHEMA_ELEMENT_NAME,
                                         singular = "Mod Scheltens",
                                         plural = "Mod Scheltens"),
                    @XnatDataModel(value = CndaModifiedscheltenspvregion.SCHEMA_ELEMENT_NAME,
                                         singular = "Mod Scheltens PV Region",
                                         plural = "Mod Scheltens PV Regions"),
                    @XnatDataModel(value = CndaModifiedscheltensregion.SCHEMA_ELEMENT_NAME,
                                         singular = "Mod Scheltens Region",
                                         plural = "Mod Scheltens Regions"),                                                            
                    @XnatDataModel(value = CndaPettimecoursedata.SCHEMA_ELEMENT_NAME,
                                         singular = "PET Timecourse",
                                         plural = "PET Timecourses"), 
                    @XnatDataModel(value = CndaPettimecoursedataDuration.SCHEMA_ELEMENT_NAME,
                                         singular = "PET Timecourse Duration",
                                         plural = "PET Timecourse Durations"), 
                    @XnatDataModel(value = CndaPettimecoursedataDurationBp.SCHEMA_ELEMENT_NAME,
                                         singular = "PET Timecourse Duration BP",
                                         plural = "PET Timecourse Duration BPs"),
                    @XnatDataModel(value = CndaPettimecoursedataRegion.SCHEMA_ELEMENT_NAME,
                                         singular = "PET Timecourse Region",
                                         plural = "PET Timecourse Regions"),
                    @XnatDataModel(value = CndaPettimecoursedataRegionActivity.SCHEMA_ELEMENT_NAME,
                                         singular = "PET Timecourse Region Activity",
                                         plural = "PET Timecourse Region Activities"),                                                       
                    @XnatDataModel(value = CndaPsychometricsdata.SCHEMA_ELEMENT_NAME,
                                         singular = "PSYCH",
                                         plural = "PSYCHs"),
                    @XnatDataModel(value = CndaRadiologyreaddata.SCHEMA_ELEMENT_NAME,
                                         singular = "Radiology Read",
                                         plural = "Radiology Reads"),  
                    @XnatDataModel(value = CndaSegmentationfastdata.SCHEMA_ELEMENT_NAME,
                                         singular = "Fast Seg",
                                         plural = "Fast Segs"),
                    @XnatDataModel(value = CndaTicvideocountsdata.SCHEMA_ELEMENT_NAME,
                                         singular = "TIC Video Counts Data",
                                         plural = "TIC Video Counts Data"),
                    @XnatDataModel(value = CndaVitalsdata.SCHEMA_ELEMENT_NAME,
                                         singular = "Vitals Data",
                                         plural = "Vitals Data"),                                                                                         
                    @XnatDataModel(value = CndaVitalsdataBloodpressure.SCHEMA_ELEMENT_NAME,
                                         singular = "Vitals Data (Blood Pressure)",
                                         plural = "Vitals Data (Blood Pressure)"),
                    @XnatDataModel(value = CndaVitalsdataPulse.SCHEMA_ELEMENT_NAME,
                                         singular = "Vitals Data (Pulse)",
                                         plural = "Vitals Data (Pulse)"),
                    @XnatDataModel(value = CndaVolumetryregioninfo.SCHEMA_ELEMENT_NAME,
                                         singular = "Volumetry Region",
                                         plural = "Volumetry Regions"),/*
                    @XnatDataModel(value = FsAparcregionanalysis.SCHEMA_ELEMENT_NAME,
                                         singular = "APARC",
                                         plural = "APARCs"),                                                                                               
                    @XnatDataModel(value = FsAparcregionanalysisHemisphere.SCHEMA_ELEMENT_NAME,
                                         singular = "APARC Hemisphere",
                                         plural = "APARC Hemispheres"),
                    @XnatDataModel(value = FsAparcregionanalysisHemisphereRegion.SCHEMA_ELEMENT_NAME,
                                         singular = "APARC Hemisphere Region",
                                         plural = "APARC Hemisphere Regions"),
                    @XnatDataModel(value = FsAsegregionanalysis.SCHEMA_ELEMENT_NAME,
                                         singular = "ASEG",
                                         plural = "ASEGs"),                                                                                              
                    @XnatDataModel(value = FsAsegregionanalysisRegion.SCHEMA_ELEMENT_NAME,
                                         singular = "ASEG Region",
                                         plural = "ASEG Regions"),
                    @XnatDataModel(value = FsAutomaticsegmentationdata.SCHEMA_ELEMENT_NAME,
                                         singular = "Auto Seg",
                                         plural = "Auto Segs"),     
                    @XnatDataModel(value = FsFsdata.SCHEMA_ELEMENT_NAME,
                                         singular = "Freesurfer",
                                         plural = "Freesurfers"),     
                    @XnatDataModel(value = FsFsdataHemisphere.SCHEMA_ELEMENT_NAME,
                                         singular = "Freesurfer Hemisphere",
                                         plural = "Freesurfer Hemispheres"), 
                    @XnatDataModel(value = FsFsdataHemisphereRegion.SCHEMA_ELEMENT_NAME,
                                         singular = "Freesurfer Hemisphere Region",
                                         plural = "Freesurfer Hemisphere Regions"),                     
                    @XnatDataModel(value = FsFsdataRegion.SCHEMA_ELEMENT_NAME,
                                         singular = "Freesurfer Region",
                                         plural = "Freesurfer Regions"),
                    @XnatDataModel(value = FsLongfsdata.SCHEMA_ELEMENT_NAME,
                                         singular = "Longitudinal Freesurfer",
                                         plural = "Longitudinal Freesurfers"),                     
                    @XnatDataModel(value = FsLongfsdataHemisphere.SCHEMA_ELEMENT_NAME,
                                         singular = "Longitudinal Freesurfer Hemisphere",
                                         plural = "Longitudinal Freesurfer Hemispheres"),
                    @XnatDataModel(value = FsLongfsdataHemisphereRegion.SCHEMA_ELEMENT_NAME,
                                         singular = "Longitudinal Freesurfer Hemisphere Region",
                                         plural = "Longitudinal Freesurfer Hemisphere Regions"),
                    @XnatDataModel(value = FsLongfsdataRegion.SCHEMA_ELEMENT_NAME,
                                         singular = "Longitudinal Freesurfer Region",
                                         plural = "Longitudinal Freesurfer Regions"),
                    @XnatDataModel(value = FsLongfsdataTimepoint.SCHEMA_ELEMENT_NAME,
                                         singular = "Longitudinal Freesurfer Timepoint",
                                         plural = "Longitudinal Freesurfer Timepoints"),*/
                    @XnatDataModel(value = GeneticsGenetictestresults.SCHEMA_ELEMENT_NAME,
                                         code = "GENES",
                                         singular = "Genetic Test Result",
                                         plural = "Genetic Test Results"),                    
                    @XnatDataModel(value = GeneticsGenetictestresultsGene.SCHEMA_ELEMENT_NAME,
                                         singular = "Genetic Test Result Gene",
                                         plural = "Genetic Test Result Genes"),
                    @XnatDataModel(value = NeurocogSymptomsneurocog.SCHEMA_ELEMENT_NAME,
                                         singular = "Neurocog Symptom",
                                         plural = "Neurocog Symptoms"),                                                             
                    @XnatDataModel(value = NundaNundademographicdata.SCHEMA_ELEMENT_NAME,
                                         singular = "NUNDA Demographic Data",
                                         plural = "NUNDA Demographic Data"),
                    @XnatDataModel(value = NundaNundademographicdataRelationship.SCHEMA_ELEMENT_NAME,
                                         singular = "NUNDA Demographic Data Relationship",
                                         plural = "NUNDA Demographic Data Relationships"),
                    @XnatDataModel(value = SapssansSymptomssapssans.SCHEMA_ELEMENT_NAME,
                                         singular = "SAPSSANS Symptom",
                                         plural = "SAPSSANS Symptoms"),
                    @XnatDataModel(value = SfEncounterlog.SCHEMA_ELEMENT_NAME,
                                         singular = "SF Encounter Log",
                                         plural = "SF Encounter Logs"),
                    @XnatDataModel(value = SfEncounterlogEncounter.SCHEMA_ELEMENT_NAME,
                                         singular = "SF Encounter",
                                         plural = "SF Encounters")
                        })

public class CentralDataPlugin {
    public CentralDataPlugin() {
        _log.info("Initializing the CentralDataPlugin data models");
    }

    private static final Logger _log = LoggerFactory.getLogger(CentralDataPlugin.class);
}
