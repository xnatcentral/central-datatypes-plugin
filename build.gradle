/*
 * central-datatypes-plugin: build.gradle
 * XNAT http://www.xnat.org
 * Copyright (c) 2016-2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

import org.gradle.internal.jvm.Jvm

buildscript {
    ext {
        vXnat = '1.7.4-SNAPSHOT'
    }
    repositories {
        mavenLocal()
        mavenCentral()
        jcenter()
        maven {
            url 'https://nrgxnat.jfrog.io/nrgxnat/libs-release'
            name 'XNAT Release Repository'
        }
        maven {
            url 'https://nrgxnat.jfrog.io/nrgxnat/libs-snapshot'
            name 'XNAT Snapshot Repository'
        }
    }
    dependencies {
        classpath "io.spring.gradle:dependency-management-plugin:1.0.3.RELEASE"
        classpath "org.nrg.xnat.build:xnat-data-builder:${vXnat}"
    }
}

apply plugin: 'io.spring.dependency-management'
apply plugin: 'java'
apply plugin: 'xnat-data-builder'
apply plugin: 'jacoco'
apply plugin: 'maven'
apply plugin: 'maven-publish'
apply plugin: 'idea'
apply plugin: 'eclipse'

repositories {
    mavenLocal()
    mavenCentral()
    jcenter()
    maven {
        url 'http://dcm4che.org/maven2'
        name 'dcm4che Maven Repository'
    }
    maven {
        url 'https://nrgxnat.jfrog.io/nrgxnat/libs-release'
        name 'XNAT Release Repository'
    }
    maven {
        url 'https://nrgxnat.jfrog.io/nrgxnat/libs-snapshot'
        name 'XNAT Snapshot Repository'
    }
}

group 'org.nrg.xnat.central'
version vXnat

sourceCompatibility = 1.7
targetCompatibility = 1.7

dependencyManagement.imports {
    mavenBom "org.nrg:parent:${vXnat}"
}

dependencies {
    implementation "org.nrg.xnat:web"
    implementation "org.nrg.xnat:xnat-data-models"
    implementation "org.nrg.xdat:core"
    implementation "org.nrg:prefs"
    implementation "org.nrg:framework"

    testImplementation "junit:junit"
    testImplementation "org.springframework:spring-test"
}

sourceSets {
    main {
        java {
            srcDir 'src/main/java'
            srcDir 'build/xnat-generated/src/main/java'
        }
        resources {
            srcDir 'src/main/resources'
            srcDir 'build/xnat-generated/src/main/resources'
        }
    }
}

task sourceJar(type: Jar, dependsOn: classes) {
    classifier "sources"
    from sourceSets.main.allSource
}

task javadocJar(type: Jar, dependsOn: javadoc) {
    classifier "javadoc"
    from javadoc.destinationDir
}

publishing.publications {
    mavenJava(MavenPublication) {
        from components.java

        artifacts {
            artifact sourceJar
            artifact javadocJar
        }

        pom.withXml {
            def root = asNode()
            root.appendNode('name', 'XNAT Server')
            root.appendNode('description', 'XNAT is an open-source imaging informatics software platform dedicated to helping you perform imaging-based research. XNAT’s core functions manage importing, archiving, processing and securely distributing imaging and related study data. But its extended uses continue to evolve.')
            root.appendNode('url', 'https://bitbucket.org/xnatdev/xnat-web')
            root.appendNode('inceptionYear', '2016')

            def scm = root.appendNode('scm')
            scm.appendNode('url', 'https://bitbucket.org/xnatdev/xnat-web')
            scm.appendNode('connection', 'scm:https://bitbucket.org/xnatdev/xnat-web.git')
            scm.appendNode('developerConnection', 'scm:git://bitbucket.org/xnatdev/xnat-web.git')

            def license = root.appendNode('licenses').appendNode('license')
            license.appendNode('name', 'Simplified BSD 2-Clause License')
            license.appendNode('url', 'http://xnat.org/about/license.php')
            license.appendNode('distribution', 'repo')

            def developers = root.appendNode('developers')
            def rherrick = developers.appendNode('developer')
            rherrick.appendNode('id', 'rherrick')
            rherrick.appendNode('name', 'Rick Herrick')
            rherrick.appendNode('email', 'jrherrick@wustl.edu')
        }
    }
}

publishing.repositories {
    maven {
        credentials {
            // These properties must be set in the ~/.gradle/gradle.properties file or passed on the Gradle command
            // line in the form -PrepoUsername=foo -PrepoPassword=bar.
            username = propertyWithDefault('repoUsername', 'username')
            password = propertyWithDefault('repoPassword', 'password')
        }
        url project.version.endsWith('-SNAPSHOT') ? "https://nrgxnat.jfrog.io/nrgxnat/libs-snapshot-local" : "https://nrgxnat.jfrog.io/nrgxnat/libs-release-local"
    }
}

def propertyWithDefault(String name, Object value) {
    hasProperty(name) ? property(name) : value
}

configurations {
    all*.exclude group: 'avalon-framework'
    all*.exclude group: 'avalon-logkit'
    all*.exclude group: 'com.metaparadigm'
    all*.exclude group: 'com.sun.mail'
    all*.exclude group: 'excalibur-component'
    all*.exclude group: 'excalibur-instrument'
    all*.exclude group: 'excalibur-logger'
    all*.exclude group: 'excalibur-pool'
    all*.exclude group: 'fop'
    all*.exclude group: 'geronimo-spec'
    all*.exclude group: 'hsqldb'
    all*.exclude group: 'imagej'
    all*.exclude group: 'jamon'
    all*.exclude group: 'jakarta-regexp'
    all*.exclude group: 'org.apache.geronimo.specs'
    all*.exclude group: 'velocity'
    all*.exclude group: 'xmlrpc'
    all*.exclude group: 'ant', module: 'ant'
    all*.exclude group: 'commons-email', module: 'commons-email'
    all*.exclude group: 'edu.ucar', module: 'netcdf'
    all*.exclude group: 'javax.jms', module: 'jms'
    all*.exclude group: 'javax.mail', module: 'mail'
    all*.exclude group: 'javax.servlet', module: 'servlet-api'
    all*.exclude group: 'javax.sql', module: 'jdbc-stdext'
    all*.exclude group: 'javax.transaction', module: 'jta'
    all*.exclude group: 'jdbc', module: 'jdbc'
    all*.exclude group: 'jms', module: 'jms'
    all*.exclude group: 'jython', module: 'jython'
    all*.exclude group: 'org.nrg', module: 'nrg'
    all*.exclude group: 'net.sf.saxon', module: 'saxon'
    all*.exclude group: 'stax', module: 'stax-api'
    all*.exclude group: 'xml-apis', module: 'xml-apis'
    all*.exclude module: 'commons-beanutils-core'
    all*.exclude module: 'log4j-slf4j-impl'
    all*.exclude module: 'pipelineCNDAXNAT'
    all*.exclude module: 'slf4j-simple'
    all*.exclude group: 'com.sun.media', module: 'jai_imageio'
    all*.exclude group: 'javax.sql', module: 'jdbc-stdext'
    all*.exclude group: 'javax.transaction', module: 'jta'
}

def javaVersion = Jvm.current().javaVersion
if (javaVersion.java8Compatible || javaVersion.java9Compatible) {
    allprojects {
        tasks.withType(Javadoc) {
            options.addStringOption('Xdoclint:none', '-quiet')
        }
    }

    if (hasProperty("rt.17.jar")) {
        // Solution for bootstrap classpath warning and possible issues with compatibility with 1.7 libraries
        // was taken from this post on discuss.gradle.org: http://bit.ly/24xD9j0
        def rt17jar = property "rt.17.jar"
        logger.info "Using ${rt17jar} as the bootstrap class path jar."
        gradle.projectsEvaluated {
            tasks.withType(JavaCompile) {
                options.fork = true
                options.compilerArgs << "-XDignore.symbol.file"
                options.bootClasspath = rt17jar as String
            }
        }
    } else {
        logger.warn "No value was set for the rt.17.jar build property, but you are using a Java 8- or 9-compatible JDK. You should consider setting rt.17.jar to indicate a jar file containing the Java 1.7 run-time library:\n"
        logger.warn "\n"
        logger.warn "  ./gradlew -Prt.17.jar=rt-1.7.0_45.jar war\n"
        logger.warn "\n"
        logger.warn "You can also set this property in a gradle.properties file, either in the top level of your build folder or in the folder ~/.gradle/:\n"
        logger.warn "\n"
        logger.warn "  rt.17.jar=/home/developer/.gradle/rt.17.jar\n"
        logger.warn "\n"
        logger.warn "In some isolated instances, using a bootstrap library from a JDK version later than 1.7 can result in run-time errors.\n"
    }
} else if (!javaVersion.java7Compatible) {
    throw new BuildCancelledException("You are using a JDK version (${javaVersion}) that is not compatible with Java 7. The XNAT build will fail. Please install a JDK version of Java 7 or later.")
}

compileJava {
    options.fork = false
}

jacoco {
    toolVersion = dependencyManagement.importedProperties['jacoco.version']
}

jacocoTestReport {
    dependsOn test
    reports {
        xml.enabled = false
        csv.enabled = false
        html.destination file("${buildDir}/jacocoHtml")
    }
}

